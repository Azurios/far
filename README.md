# FAr - Finite Automata in rust

This program serves as a general tool for dealing with [finite automata](https://en.wikipedia.org/wiki/Deterministic_finite_automaton).

## Installation

Linux and Windows users can download the respective binary from the [release section](https://gitlab.com/Azurios/far/-/releases/Test).

Do note that this is a CLI program! If you are on windows and wonder, why clicking the .exe doesn't open anything, you need to use Powershell to open far.exe.

If you want to compile from source, assuming you have rust installed, you can simply clone this repository and run `cargo run` inside the directory.

## Usage

Running far without any arguments or with the `--help` flag will show an overview of all possible commands.

Run `far generate` to begin the guided generation of an automaton to be used for the other commands.

Run `far test <path_to_automaton> <word>` to test if a given automaton accepts a given word, i.e.
```
far test abmachine.json abbbaab
```
would test if the automaton saved in `abmachine.json` in the current directory accepts the word `abbbaab` or not.

If you wish to test multiple words at one time, you can run `far testfile <path_to_automaton> <path_to_wordlist>`.

To generate a dotfile for use with graphviz, for example [this site](http://viz-js.com/), run `far dot <path_to_automaton>`.
```
far dot abmachine.json
```
will create a file called `abmachine.dot` that you can then use with graphviz.

For the testing commands, you might want more verbose output to follow the traversal of states. For this, pass the `-v` or `--verbose` flag to far, i.e.
```
far -v test abmachine.json abbbaab
```
