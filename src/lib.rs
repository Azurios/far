use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::fmt;
pub mod styles;

#[macro_use]
extern crate log;

type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Clone)]
pub enum Error {
    NonDetTransition((u32, u32, char), (u32, u32, char)),
    NonExistentState(u32),
    NotInAlphabet(char),
    // TODO: use this error :(
    IllegalCharacter(char),
    InconsistentFA(InconsistencyError),
}

#[derive(Debug, Clone)]
pub enum InconsistencyError {
    MissingStartState,
    MissingTransition(u32, char),
}
pub trait Fa {
    fn add_state(&mut self);
    fn add_states(&mut self, no: u32);
    fn add_transition(&mut self, c: char, first: u32, second: u32) -> Result<()>;
    // this REPLACES startid if there has already been one.
    fn assign_startid(&mut self, id: u32) -> Result<()>;
    fn assign_accepting(&mut self, ids: Vec<u32>) -> Result<()>;
    fn assign_alphabet(&mut self, chars: Vec<char>) -> Result<()>;
    fn is_consistent(&self) -> (bool, Option<Error>);
    fn test(&self, word: &str) -> Result<(bool, Vec<u32>)>;
    fn unsafe_test(&self, word: &str) -> Result<(bool, Vec<u32>)>;
    fn export_to_json(&mut self) -> String;
    fn export_to_dot(&self, file: &mut std::fs::File) -> std::result::Result<(), std::io::Error>;
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self{
    Error::NonDetTransition(first , second) => write!(f, "You tried adding a non-deterministic transition to a deterministic automaton. You tried to add {:?}, which conflicts with the existing transition {:?}", first, second),
    Error::NonExistentState(id) => write!(f, "There is no state with ID {}, you are likely out of bounds", id),
    Error::NotInAlphabet(character) => write!(f, "The character {} is not part of the this automaton's alphabet", character),
    Error::IllegalCharacter(character) => write!(f, "You have used an illegal character: {} is reserved and can't be used", character),
    Error::InconsistentFA(inner_error) => write!(f, "This automaton is inconsistent: {}", inner_error),
        }
    }
}

impl fmt::Display for InconsistencyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InconsistencyError::MissingStartState => {
                write!(f, "This automaton has no starting state")
            }
            InconsistencyError::MissingTransition(state, character) => write!(
                f,
                "This automaton is missing a transition for character {} from state {}",
                character, state
            ),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Nfa {
    //epsilon is the char '_'
    alphabet: Vec<char>,
    nextid: u32,
    states: HashMap<u32, State>,
    startid: Option<u32>,
    acceptids: Vec<u32>,
    #[serde(with = "serde_with::rust::hashmap_as_tuple_list")]
    transitions: HashMap<(char, u32), Vec<u32>>,
    // only for (de)-serializing for unsafe tests
    isconsistent: bool,
}

impl<'a> dot::Labeller<'a, u32, (char, u32, u32)> for Dfa {
    fn graph_id(&self) -> dot::Id {
        dot::Id::new("FA").unwrap()
    }
    fn node_id(&self, id: &u32) -> dot::Id {
        dot::Id::new(format!("q{}", id)).unwrap()
    }
    fn edge_label(&'a self, e: &(char, u32, u32)) -> dot::LabelText<'a> {
        dot::LabelText::LabelStr(Cow::Owned(e.0.to_string()))
    }
    fn node_shape(&'a self, id: &u32) -> Option<dot::LabelText<'a>> {
        if self.states.get(id).unwrap().accepting {
            Some(dot::LabelText::LabelStr(Cow::Owned("doublecircle".into())))
        } else {
            Some(dot::LabelText::LabelStr(Cow::Owned("circle".into())))
        }
    }
    fn node_color(&'a self, id: &u32) -> Option<dot::LabelText<'a>> {
        if self.states.get(id).unwrap().starting {
            Some(dot::LabelText::LabelStr(Cow::Owned("blue".into())))
        } else {
            None
        }
    }
}

impl<'a> dot::GraphWalk<'a, u32, (char, u32, u32)> for Dfa {
    fn nodes(&self) -> dot::Nodes<u32> {
        (0..self.nextid).collect()
    }
    fn edges(&self) -> dot::Edges<(char, u32, u32)> {
        self.transitions
            .iter()
            .map(|x| (x.0 .0, x.0 .1, x.1.to_owned()))
            .collect()
    }
    fn source(&self, e: &(char, u32, u32)) -> u32 {
        e.1
    }
    fn target(&self, e: &(char, u32, u32)) -> u32 {
        e.2
    }
}

impl<'a> dot::Labeller<'a, u32, (char, u32, u32)> for Nfa {
    fn graph_id(&self) -> dot::Id {
        dot::Id::new("NFA").unwrap()
    }
    fn node_id(&self, id: &u32) -> dot::Id {
        dot::Id::new(format!("q{}", id)).unwrap()
    }
    fn edge_label(&'a self, e: &(char, u32, u32)) -> dot::LabelText<'a> {
        if e.0 == '_' {
            dot::LabelText::LabelStr("&epsilon;".into())
        } else {
            dot::LabelText::LabelStr(Cow::Owned(e.0.to_string()))
        }
    }
    fn node_shape(&'a self, id: &u32) -> Option<dot::LabelText<'a>> {
        if self.states.get(id).unwrap().accepting {
            Some(dot::LabelText::LabelStr(Cow::Owned("doublecircle".into())))
        } else {
            Some(dot::LabelText::LabelStr(Cow::Owned("circle".into())))
        }
    }
    fn node_color(&'a self, id: &u32) -> Option<dot::LabelText<'a>> {
        if self.states.get(id).unwrap().starting {
            Some(dot::LabelText::LabelStr(Cow::Owned("blue".into())))
        } else {
            None
        }
    }
}

impl<'a> dot::GraphWalk<'a, u32, (char, u32, u32)> for Nfa {
    fn nodes(&self) -> dot::Nodes<u32> {
        (0..self.nextid).collect()
    }
    fn edges(&self) -> dot::Edges<(char, u32, u32)> {
        let mut res = Vec::new();
        for element in self.transitions.iter() {
            for target in element.1 {
                res.push((element.0 .0, element.0 .1, target.to_owned()));
            }
        }
        Cow::Owned(res)
    }
    fn source(&self, e: &(char, u32, u32)) -> u32 {
        e.1
    }
    fn target(&self, e: &(char, u32, u32)) -> u32 {
        e.2
    }
}

#[derive(Serialize, Deserialize)]
pub struct Dfa {
    // nextid conviniently always holds how many states there are
    alphabet: Vec<char>,
    nextid: u32,
    states: HashMap<u32, State>,
    startid: Option<u32>,
    acceptids: Vec<u32>,
    #[serde(with = "serde_with::rust::hashmap_as_tuple_list")]
    transitions: HashMap<(char, u32), u32>,
    // only for (de)-serializing for unsafe tests
    isconsistent: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct State {
    id: u32,
    accepting: bool,
    starting: bool,
    epsclosure: RefCell<HashSet<u32>>,
}
impl Dfa {
    pub fn new() -> Dfa {
        Dfa {
            alphabet: Vec::new(),
            nextid: 0,
            states: HashMap::new(),
            startid: None,
            acceptids: Vec::new(),
            transitions: HashMap::new(),
            isconsistent: false,
        }
    }
    pub fn import_from_json(json: &str) -> std::result::Result<Self, serde_json::Error> {
        serde_json::from_str(json)
    }
}
impl Fa for Dfa {
    fn export_to_json(&mut self) -> String {
        self.isconsistent = self.is_consistent().0;
        serde_json::to_string(&self).unwrap()
    }

    fn add_state(&mut self) {
        let mut newstate = State::new();
        newstate.id = self.nextid;
        self.nextid += 1;
        self.states.insert(newstate.id, newstate);
    }

    fn add_states(&mut self, no: u32) {
        for _ in 0..no {
            self.add_state();
        }
    }

    fn add_transition(&mut self, c: char, first: u32, second: u32) -> Result<()> {
        if !(first < self.nextid) {
            return Err(Error::NonExistentState(first));
        }
        if !(second < self.nextid) {
            return Err(Error::NonExistentState(second));
        }
        if !(self.alphabet.contains(&c)) {
            return Err(Error::NotInAlphabet(c));
        }
        if let Some(q) = self.transitions.get(&(c, first)) {
            if q == &second {
                Ok(())
            } else {
                return Err(Error::NonDetTransition((first, second, c), (first, *q, c)));
            }
        } else {
        trace!("seems okay");
            // new transition
            self.transitions.insert((c, first), second);
            Ok(())
        }
    }

    fn assign_startid(&mut self, id: u32) -> Result<()> {
        if id >= self.nextid {
            return Err(Error::NonExistentState(id));
        }
        // if let Some(q) = self.startid {
        // TODO: not sure if this should error?
        // Err(format!("A starting state is already known: {}", q))
        // }
        else {
            self.startid = Some(id);
            self.states.get_mut(&id).unwrap().assign_starting();
            Ok(())
        }
    }

    fn assign_accepting(&mut self, ids: Vec<u32>) -> Result<()> {
        for id in &ids {
            if !(id < &self.nextid) {
                return Err(Error::NonExistentState(*id));
            }
        }
        for id in ids {
            self.acceptids.push(id);
            self.states.get_mut(&id).unwrap().assign_accepting();
        }
        Ok(())
    }

    fn assign_alphabet(&mut self, chars: Vec<char>) -> Result<()> {
        for char in &chars {
            if char == &'_' {
                return Err(Error::IllegalCharacter('_'));
            }
        }
        self.alphabet = chars;
        Ok(())
    }

    fn is_consistent(&self) -> (bool, Option<Error>) {
        if self.startid.is_none() {
            return (false, Some(Error::InconsistentFA(InconsistencyError::MissingStartState)));
        }
        for id in 0..self.nextid {
            for c in self.alphabet.iter() {
                if self.transitions.get(&(*c, id)).is_none() {
                    return (false, Some(Error::InconsistentFA(InconsistencyError::MissingTransition(id, *c))));
                }
            }
        }
        return (true, None);
    }

    fn test(&self, word: &str) -> Result<(bool, Vec<u32>)> {
        let consistency = self.is_consistent();
        if !consistency.0 {
         return Err(consistency.1.unwrap());
        }
        for c in word.chars(){
            if !self.alphabet.contains(&c){
                return Err(Error::NotInAlphabet(c));
            }
        }
        self.unsafe_test(word)
    }

    fn unsafe_test(&self, word: &str) -> Result<(bool, Vec<u32>)> {
        let mut curr = self.startid.unwrap();
        trace!("starting in state {}", styles::DEBUG.apply_to(curr));
        for c in word.chars() {
            curr = *self.transitions.get(&(c, curr)).unwrap();
        trace!("read character {}, transitioned to state {}", styles::DEBUG.apply_to(c), styles::DEBUG.apply_to(curr));
        }
        Ok((self.states.get(&curr).unwrap().accepting, vec![curr]))
    }

    fn export_to_dot(&self, file: &mut std::fs::File) -> std::result::Result<(), std::io::Error> {
        info!("Test");
        dot::render(self, file)
    }
}

impl State {
    fn new() -> State {
        State {
            id: 0,
            accepting: false,
            starting: false,
            epsclosure: RefCell::new(HashSet::new()),
        }
    }

    fn assign_accepting(&mut self) {
        self.accepting = true;
    }

    fn assign_starting(&mut self) {
        self.starting = true;
    }
}

impl Nfa {
    pub fn new() -> Nfa {
        Nfa {
            alphabet: Vec::new(),
            nextid: 0,
            states: HashMap::new(),
            startid: None,
            acceptids: Vec::new(),
            transitions: HashMap::new(),
            isconsistent: false,
        }
    }

    pub fn import_from_json(json: &str) -> std::result::Result<Self, serde_json::Error> {
        serde_json::from_str(json)
    }
}

impl Fa for Nfa {
    fn export_to_json(&mut self) -> String {
        self.isconsistent = self.is_consistent().0;
        serde_json::to_string(self).unwrap()
    }
    fn add_state(&mut self) {
        let mut newstate = State::new();
        newstate.id = self.nextid;
        self.nextid += 1;
        self.states.insert(newstate.id, newstate);
    }

    fn add_states(&mut self, no: u32) {
        for _ in 0..no {
            self.add_state();
        }
    }

    fn add_transition(&mut self, c: char, first: u32, second: u32) -> Result<()> {
        if !(first < self.nextid) {
            return Err(Error::NonExistentState(first));
        }
        if !(second < self.nextid) {
            return Err(Error::NonExistentState(second));
        }
        if !(self.alphabet.contains(&c) || c == '_') {
            return Err(Error::NotInAlphabet(c));
        }
        // if !self.transitions.contains_key(&(c, first)) {
        //     self.transitions.insert((c, first), Vec::new());
        // }
        trace!("seems okay");
        self.transitions
            .entry((c, first))
            .or_insert_with(Vec::new)
            .push(second);
        if c == '_' {
            let firststate = self.states.get(&first).unwrap();
            firststate.epsclosure.borrow_mut().insert(second);
            firststate
                .epsclosure
                .borrow_mut()
                .extend(self.states.get(&second).unwrap().epsclosure.borrow().iter());
        }
        Ok(())
    }

    fn assign_startid(&mut self, id: u32) -> Result<()> {
        assert!(id < self.nextid);
        // if let Some(q) = self.startid {
        // debatable if acceptable for NFA
        // return Err(format!("Another starting state is already known: {}", q));
        // }
        // else {
        self.startid = Some(id);
        self.states.get_mut(&id).unwrap().assign_starting();
        Ok(())
        //}
    }

    fn assign_accepting(&mut self, ids: Vec<u32>) -> Result<()> {
        for id in &ids {
            if !(id < &self.nextid) {
                return Err(Error::NonExistentState(*id));
            }
        }
        for id in ids {
            self.acceptids.push(id);
            self.states.get_mut(&id).unwrap().assign_accepting();
        }
        Ok(())
    }

    fn assign_alphabet(&mut self, chars: Vec<char>) -> Result<()> {
        for char in &chars {
            if char == &'_' {
                return Err(Error::IllegalCharacter('_'));
            }
        }
        self.alphabet = chars;
        Ok(())
    }

    fn is_consistent(&self) -> (bool, Option<Error>) {
        if self.startid.is_none() {
            return (false, Some(Error::InconsistentFA(InconsistencyError::MissingStartState)));
        }
        return (true, None);
    }

    fn test(&self, word: &str) -> Result<(bool, Vec<u32>)> {
        let consistency = self.is_consistent();
        if !consistency.0 {
         return Err(consistency.1.unwrap());
        }
        for c in word.chars(){
            if !self.alphabet.contains(&c){
                return Err(Error::NotInAlphabet(c));
            }
        }
        self.unsafe_test(word)
}

    fn unsafe_test(&self, word: &str) -> Result<(bool, Vec<u32>)> {
        // epsilons suck :(
        // not anymore :)
        let mut states = HashSet::new();
        states.insert(self.startid.unwrap());
        states.extend(
            self.states
                .get(&self.startid.unwrap())
                .unwrap()
                .epsclosure
                .borrow()
                .iter(),
        );
        trace!("Starting in state(s): {:?}", styles::DEBUG.apply_to(&states));
        for c in word.chars() {
            let mut next: HashSet<u32> = HashSet::new();
            let mut closures: HashSet<u32> = HashSet::new();
            for state in &states {
                next.extend(
                    self.transitions
                        .get(&(c, *state))
                        .unwrap_or(&Vec::new())
                        .iter(),
                );
            }
            for n in &next {
                closures.extend(self.states.get(&n).unwrap().epsclosure.borrow().iter());
            }
            next.extend(closures.iter());
            trace!("After reading character {}, possible states are {:?}", styles::DEBUG.apply_to(&c), styles::DEBUG.apply_to(&next));
            states = next;
            //println!("currently possible states, in loop: {:?}", states);
        }
        //println!("currently possible states, after loop: {:?}", states);
        for s in &states {
            if self.states.get(&s).unwrap().accepting {
                return Ok((true, states.into_iter().collect()));
            }
        }
        Ok((false, states.into_iter().collect()))
    }

    fn export_to_dot(&self, file: &mut std::fs::File) -> std::result::Result<(), std::io::Error> {
        info!("another test");
        dot::render(self, file)
    }
}

#[cfg(test)]
#[allow(unused_must_use)]
mod tests {
    use super::*;

    #[test]
    fn consistent_state_count() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        assert_eq!(fa.nextid, 3);
        fa.add_state();
        fa.add_state();
        assert_eq!(fa.nextid, 5);
    }

    #[test]
    fn assign_start() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        assert_eq!(fa.startid, None);
        fa.assign_startid(0);
        assert_eq!(fa.startid, Some(0));
    }

    /*
    #[test]
    fn assign_start_twice() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        fa.assign_startid(0);
        assert!(fa.assign_startid(1).is_err());
    }
    */

    #[test]
    fn assign_oob_transition() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        assert!(fa.add_transition('a', 0, 3).is_err());
    }

    #[test]
    fn assign_oob_start() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        assert!(fa.assign_startid(3).is_err());
    }

    #[test]
    fn non_deterministic_transition_in_dfa() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        fa.add_transition('a', 0, 1);
        assert!(fa.add_transition('a', 0, 2).is_err());
    }

    #[test]
    fn duplicate_transition() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        fa.assign_alphabet(vec!['a']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('a', 0, 1);
    }

    #[test]
    fn assign_accepting_test() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        fa.assign_accepting(vec![0, 2]);
        assert_eq!(fa.acceptids.len(), 2);
        for state in fa.states.values() {
            assert_eq!(state.accepting, fa.acceptids.contains(&state.id));
        }
    }
    #[test]
    fn states_starting_consistent() {
        let mut fa = Dfa::new();
        fa.add_states(9);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        for state in fa.states.values() {
            assert!((state.id == 0 && state.starting) || (state.id != 0 && !state.starting));
        }
    }

    #[test]
    fn simple_check() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        fa.assign_alphabet(vec!['a']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('a', 1, 2);
        fa.add_transition('a', 2, 0);
        assert!(!fa.test("aaaa").unwrap().0);
        assert!(fa.test("aaa").unwrap().0);
        assert!(fa.test("aa").unwrap().0);
        assert!(!fa.test("a").unwrap().0);
        assert!(fa.test("").unwrap().0);
    }

    #[test]
    fn consistency_true() {
        let mut fa = Dfa::new();
        fa.add_states(3);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        fa.assign_alphabet(vec!['a', 'b']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('a', 1, 2);
        fa.add_transition('a', 2, 0);
        fa.add_transition('b', 0, 2);
        fa.add_transition('b', 2, 1);
        fa.add_transition('b', 1, 0);
        assert!(fa.is_consistent().0);
    }

    #[test]
    fn json_out() {
        let mut fa = Nfa::new();
        fa.add_states(3);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        fa.assign_alphabet(vec!['a', 'b']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('a', 0, 2);
        fa.add_transition('a', 1, 2);
        fa.add_transition('a', 2, 0);
        fa.add_transition('b', 0, 2);
        fa.add_transition('b', 2, 1);
        fa.add_transition('b', 1, 0);
        let string = fa.export_to_json();
        println!("{}", string);
        let copy = Nfa::import_from_json(&string).unwrap();
        assert!(copy.transitions.len() == fa.transitions.len());
    }
    #[test]
    fn dot_out() {
        let mut fa = Nfa::new();
        fa.add_states(5);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        fa.assign_alphabet(vec!['a', 'b']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('a', 1, 2);
        fa.add_transition('a', 2, 0);
        fa.add_transition('b', 0, 2);
        fa.add_transition('b', 2, 1);
        fa.add_transition('b', 1, 0);
        fa.add_transition('b', 2, 3);
        fa.add_transition('a', 2, 3);
        fa.add_transition('b', 3, 4);
        fa.add_transition('a', 3, 4);
        fa.add_transition('b', 4, 3);
        fa.add_transition('a', 4, 3);
        fa.add_transition('_', 0, 2);
        fa.add_transition('_', 0, 1);
        fa.export_to_dot(&mut std::fs::File::create("/dev/null").unwrap())
            .unwrap();
    }

    #[test]
    fn fna_transitions() {
        let mut fa = Nfa::new();
        fa.add_states(5);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        fa.assign_alphabet(vec!['a', 'b']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('a', 0, 2);
        assert!(fa.transitions.get(&('a', 0)).unwrap().len() == 2);
    }

    #[test]
    fn nfa_test() {
        let mut fa = Nfa::new();
        fa.add_states(5);
        fa.assign_accepting(vec![0, 2]);
        fa.assign_startid(0);
        fa.assign_alphabet(vec!['a', 'b']);
        fa.add_transition('a', 0, 1);
        fa.add_transition('_', 0, 1);
        fa.add_transition('a', 1, 2);
        fa.add_transition('a', 0, 2);
        assert!(fa.test("a").unwrap().0);
        assert!(fa.test("aa").unwrap().0);
        assert!(!fa.test("ab").unwrap().0);
    }

    //TODO: Rankdir=LR in graphviz output
}
